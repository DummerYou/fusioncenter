import styles from './index.css';
import React, { PureComponent, Fragment } from 'react';
import { Icon, Input, Button, Form, Checkbox, message } from 'antd';
import Link from 'umi/link';
import router from 'umi/router';
import { connect } from 'dva';
import { mobileValidator, setStorage, getStorage, rmStorage, decrypt ,setCook} from '../../services/utils';
const FormItem = Form.Item;

@connect(({ login, loading }) => ({
  login
}))
@Form.create()
class Login extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showType: '1',
      rememberMe: true,
      downSeconds: 0,
      errText:"",
    };
  }

  componentDidMount() {
    const {
      location: { query },
    } = this.props;
    this.setState({ showType: query.type || '1' });
    let loginInfo = getStorage('loginInfo');
    if (loginInfo && loginInfo.name) {
      this.setState({
        rememberMe: true,
      });
      this.props.form.setFieldsValue({ name: loginInfo.name, password: loginInfo.password });
    }
    // console.log(decrypt('fvgRj/8wWBezTAx0WNzzBQ=='));
  }

  componentWillUnmount() {
    this.downTimer && clearTimeout(this.downTimer);
  }

  downTimer = '';
  setErr = () => {
    
    this.setState({
      errText: "",
    });
  }
  //登录表单提交
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields(['name', 'password'], (err, values) => {
      console.log(values);
      if (!err) {
        const { rememberMe } = this.state;
        const { dispatch } = this.props;
        values.rememberMe = rememberMe;
        const params = {
          username: values.name,
          password: values.password,
        };

        if (rememberMe) {
          setStorage('loginInfo', {
            name: values.name,
            password: values.password,
          });
        } else {
          rmStorage('loginInfo');
        }
        // if(true){
          // this.setState({
          //   errText: false,
          // });
          // e.preventDefault();
          // this.props.form.validateFields(['password'], (err, values) => {
          //   console.log(values);
          // })
          
        // }else{
          // router.push('/home');
        // }
        router.push('/home');
        dispatch({
          type: 'login/login',
          payload: params,
        }).then(res => {
          if (res.code == 200) {
            setStorage('userInfo', res.data);
            setCook("User-Token",res.data.token)
            if (rememberMe) {
              setStorage('loginInfo', {
                name: values.name,
                password: values.password,
              });
            } else {
              rmStorage('loginInfo');
            }
            router.push('/home');
          } else if(res.code == 400) {
               this.setState({
                errText: res.message,
              });
          }else{
            this.setState({
              errText: res.message,
            });
          }
        });
      }
    });
  };

  onRef = ref => {
    this.child = ref;
  };
  //倒计时
  downTime = time => {
    time--;
    this.setState({ downSeconds: time });
    if (time < 1) {
      return;
    }
    this.downTimer = setTimeout(() => {
      this.downTime(time);
    }, 1000);
  };
  onChange = val => {
    this.setState({ rememberMe: !this.state.rememberMe });
  };

  render() {
    const {
      form: { getFieldDecorator, getFieldValue }
    } = this.props;
    const { defName, defPass, downSeconds, showType,errText } = this.state;
    console.log(defName, defPass);
    return (
      <div className={styles.main}>
        <div className={styles.login}>
          <div className={styles.loginTitle1}><div className={styles.loginTitleImg}  ></div><span>百胜-融合中心</span></div>
          <div className={styles.loginTitle2}>欢迎登陆</div>
          <Form onSubmit={this.handleSubmit} className={styles.from}>
            <FormItem label="用户名" className={'fromLogin fromLogintop'}>
              {getFieldDecorator('name', {
                rules: [{ required: true, message: '请输入用户名' }],
              })(
                <Input
                  size="large"
                  suffix={<span></span>}
                  placeholder="请输入用户名"
                />,
              )}
            </FormItem>
            <FormItem label="密码" className={'fromLogin'}>
              {getFieldDecorator('password', {
                // initialValue: defPass,
                rules: [{ required: true, message: '请输入密码' }],
              })(
                <Input
                  size="large"
                  suffix={<span></span>}
                  type="password"
                  onChange={() => this.setErr()}
                  placeholder="请输入密码"
                />,
              )}
              {
                errText?<div className={"explain"}>{errText}</div>:null
              }
            </FormItem>
            <div className={styles.loginCheckbox}>
              <Checkbox checked={this.state.rememberMe} onChange={() => this.onChange()}>
                记住我的登陆信息
              </Checkbox>
            </div>
            <Button
              type="primary"
              size="large"
              htmlType="submit"
              className={styles.loginFormButton}
            >
              立即登录
            </Button>
          </Form>
        </div>
      </div>
    );
  }
}

export default Login;
