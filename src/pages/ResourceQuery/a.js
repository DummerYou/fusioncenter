let dataJson = {
    "code": 200,
    "message": "请求成功",
    "data": {
        "current": 1,
        "size": 1,
        "total": 4,
        "resourceColumns": [
            "K8S","K8S6"
        ],
        "results": [
            {
                "scene": "service-controller",
                "subAppId": "prophet-resource-sagesdk140-rl-model-analyze-808-2ced",
                "user": "4pdadmin",
                "workspace": "cyf",
                "createTime": "2021-08-23 03:09:32",
                "resources": [
                    {
                        "updatedTime": "2021-08-23 03:09:32",
                        "name": "K8S",
                        "createdTime": "2021-08-23 03:09:32",
                        "value": [
                            {
                                "metricsName": "mem",
                                "value": "2.0GB"
                            },
                            {
                                "metricsName": "cpu",
                                "value": "1"
                            }
                        ]
                    },{
                        "updatedTime": "2021-08-25 03:09:32",
                        "name": "K8S6",
                        "createdTime": "2021-08-25 03:09:32",
                        "value": [
                            {
                                "metricsName": "mem",
                                "value": "2.0GB"
                            },
                            {
                                "metricsName": "cpu",
                                "value": "2"
                            }
                        ]
                    }
                ]
            }
        ]
    }
}


export default dataJson;