import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import searchCss from '../../assets/search.css';
import { Input, Button, Icon, DatePicker, Table } from 'antd';
import { setCook } from '../../services/utils';
import { connect } from 'dva';
import moment from 'moment';
import dataJson from './a.js';
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

@connect(({ other }) => ({
  other,
}))
class ResourceQuery extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id ? this.props.id : null,
      name: '',
      subappId: '',
      createdTime:"",
      current: 1,
      size: 10,
      total: 0,
      resourceColumns: [],
      results: [],
      user:"",
      workspace:"",
      scene:"",
      platform:"",
      startTime:null,
      endTime:null,
    };
  }

  componentDidMount() {
    const { id } = this.state;
    this.search();
  }
  onChange(date, dateString) {
    console.log(date, dateString);
  }
  onChangeTime = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      createdTime: date,
    });
  };
  onChangeTime1 = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      startTime: date,
    });
  };
  onChangeTime2 = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      endTime: date,
    });
  };

  componentWillUnmount() {}
  reset = () => {
    this.setState({
      name: '',
      subappId: '',
      createdTime: "",
      current: 1,
      size: 10,
      total: 0,
      resourceColumns: [],
      results: [],
      user:"",
      workspace:"",
      scene:"",
      platform:"",
      startTime:null,
      endTime:null,
    },() => {
      this.search();
    });
  };
  search = num => {
    console.log('search');
    const { dispatch } = this.props;
    const { name, subappId, createdTime, current, size ,user, workspace, scene, platform, startTime, endTime,} = this.state;
    
    console.log(current);
    const search = {
      user, workspace, scene, platform,
      startTime: startTime ? moment(startTime).format('YYYY-MM-DD hh:mm:ss') : '',
      endTime: endTime ? moment(endTime).format('YYYY-MM-DD hh:mm:ss') : '',
      current: num ? num : current,
      size,
    };
    console.log(search);
    // var data = dataJson.data;
    // this.setState({
    //   current: data.current,
    //   size: data.size,
    //   total: data.total,
    //   resourceColumns: data.resourceColumns,
    //   results: data.results,
    // });
      dispatch({
        type: 'other/queryFusionResource',
        payload: search,
      }).then(res => {
        if (res.code == 200) {
          var data = res.data;
          this.setState({
            current: data.current,
            size: data.size,
            total: data.total,
            resourceColumns: data.resourceColumns,
            results: data.results,
          });
        } else {
        }
      });
  };
  render() {
    const { current, size, total, resourceColumns, results } = this.state;
    
    setCook('headerName', '资源查询');
    let list = results.map(v => {
      console.log(v);
      if (v.resources) {
        v.resources.forEach(element => {
          var value = [];
          element.value.forEach(val => {
            value.push(val.metricsName + '：' + val.value);
          });
          v[element.name] = value.join('，');
          v[element.name + 'createdTime'] = element.createdTime;
          v[element.name + 'updatedTime'] = element.updatedTime;
        });
      }
      return v;
    });

    var columns = [
      {
        title: '租户',
        dataIndex: 'workspace',
        align: 'center',
      },
      {
        title: '场景',
        dataIndex: 'scene',
        align: 'center',
      },
      {
        title: '创建用户',
        dataIndex: 'user',
        align: 'center',
      },
    ];
    if(resourceColumns){
      for (const key of resourceColumns) {
        console.log(key);
        columns.push({
          title: key + '资源',
          dataIndex: key,
          align: 'center',
        });
        columns.push({
          title: '开始时间',
          dataIndex: key + 'createdTime',
          align: 'center',
        });
        columns.push({
          title: '结束时间',
          dataIndex: key + 'updatedTime',
          align: 'center',
        });
      }
    }

    const pagination = {
      current: current,
      pageSize: size,
      total: total,
      showQuickJumper: true,
      onChange: (date, dateString) => {
        console.log(date);
        this.setState(
          {
            current: date,
          },
          this.search(date),
        );
      },
    };
    return (
      <div className={styles.bmq}>
        <div className={styles.header}>
          <div className={styles.headerL}>资源查询列表</div>
        </div>
        <div className={searchCss.search}>
            <div className={searchCss.searchItem}>
              <span>租户：</span>
              <Input
                className={searchCss.serachIpt}
                placeholder="请输入租户"
                value={this.state.workspace}
                onChange={e => this.setState({ workspace: e.target.value })}
                suffix={<Icon type="search" />}
              />
            </div>
            <div className={searchCss.searchItem}>
              <span>创建用户：</span>
              <Input
                className={searchCss.serachIpt}
                placeholder="请输入创建用户"
                value={this.state.user}
                onChange={e => this.setState({ user: e.target.value })}
                suffix={<Icon type="search" />}
              />
            </div>
            <div className={searchCss.searchItem}>
              <span>场景：</span>
              <Input
                className={searchCss.serachIpt}
                placeholder="请输入场景"
                value={this.state.scene}
                onChange={e => this.setState({ scene: e.target.value })}
                suffix={<Icon type="search" />}
              />
            </div>
            <div className={searchCss.searchItem}>
              <span>资源类型：</span>
              <Input
                className={searchCss.serachIpt}
                placeholder="请输入资源类型"
                value={this.state.platform}
                onChange={e => this.setState({ platform: e.target.value })}
                suffix={<Icon type="search" />}
              />
            </div>
        </div>
        <div className={searchCss.search2}>
            <div className={searchCss.searchItem}>
              <span>开始时间：</span>
              <DatePicker  className={styles.picker} value={this.state.startTime} onChange={this.onChangeTime1} />
            </div>
            <div className={searchCss.searchItem}>
              <span>结束时间：</span>
              <DatePicker  className={styles.picker} value={this.state.endTime} onChange={this.onChangeTime2} />
            </div>
            <div className={searchCss.searchItem + ' ' + styles.searchBtn}>
              <Button className={searchCss.r20} onClick={this.reset}>
                重置
              </Button>
              <Button type="primary" onClick={() => this.search()}>
                搜索
              </Button>
            </div>
        </div>
        <div className={styles.table}>
          <Table columns={columns} dataSource={list} pagination={pagination} size="middle" />
        </div>
      </div>
    );
  }
}
export default ResourceQuery;
