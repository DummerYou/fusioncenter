import styles from './index.css';
import React, { PureComponent, Fragment } from 'react';
import { Icon, Input, Button, Form, Checkbox, message, Tabs } from 'antd';
import Link from 'umi/link';
import router from 'umi/router';
import { connect } from 'dva';
import ResourceQuery from "../../components/ResourceQuery"
import BusinessMonitoringQuery from "../../components/BusinessMonitoringQuery"
import FeatureCenter from "../../components/FeatureCenter"

import { mobileValidator, setStorage, getStorage, rmStorage, decrypt } from '../../services/utils';
const FormItem = Form.Item;
const { TabPane } = Tabs;

@connect(({ login, loading }) => ({
  login,
  submitting: loading.effects['login/login'] || loading.effects['login/register'],
}))
@Form.create()
class Home extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showType: '1',
    };
  }

  componentDidMount() {
    // console.log(decrypt('fvgRj/8wWBezTAx0WNzzBQ=='));
  }

  componentWillUnmount() {
  }




  render() {
    return (
      <div className={styles.main}>
        
        <FeatureCenter/>
      </div>
    );
  }
}

export default Home;
