import styles from './index.css';
import React, { PureComponent, Fragment } from 'react';
import {
  Icon,
  Input,
  Button,
  Form,
  Checkbox,
  message,
  Tabs,
  Breadcrumb,
  Row,
  Col,
  Typography,
  Table,
} from 'antd';
import Link from 'umi/link';
import router from 'umi/router';
import { connect } from 'dva';
import dataJson from './aa.js';
import moment from 'moment';

import { mobileValidator, setStorage, getStorage, rmStorage, decrypt } from '../../services/utils';
const FormItem = Form.Item;
const { TabPane } = Tabs;
const { Paragraph } = Typography;

@connect(({ feature }) => ({
  feature,
}))
@Form.create()
class featuresNew extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      schema: {
        fieldSchema: [],
      },
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    const connectionConfigSchema = JSON.parse(sessionStorage.getItem('connectionConfigSchema'));
    if(!connectionConfigSchema){
      router.push('/home');
      message.error('获取数据错误');
    }else{
      this.setState({
        schema: connectionConfigSchema,
        schemaList: connectionConfigSchema.fieldSchema,
      });
    }

  }

  componentWillUnmount() {}

  hasOk = val => {
    const { dispatch } = this.props;
    const { schema, schemaList } = this.state;
    const connectionConfigItem = JSON.parse(sessionStorage.getItem('connectionConfigItem'));
    // console.log(connectionConfigItem);
    // let connectss = connectionConfigItem.connectionConfig || []
    // let connectss2 = {}
    // connectss.forEach(v => {
    //   connectss2[v.field] = v.value
    // });
    const data = {
      
      connectionConfig: sessionStorage.getItem('connectionConfig'),
      // connectionConfig: JSON.stringify(connectss2),
      storageEngine: connectionConfigItem.storageEngine1,
      connectionType: connectionConfigItem.storageEngineTxt,
      viewId: connectionConfigItem.featureId,
      name: schema.name,
      schemaData: schemaList,
      dataSize:schema.size,
      description:schema.description,
      recordNum:schema.lineNum,
      storageUrl:schema.storageUrl,
    };
    dispatch({
      type: 'feature/registerFusionFeature',
      payload: data,
    }).then(res => {
      console.log(res);
      if (res.code === 200) {
        router.push('/home');
        message.success('新增特征成功');
      } else {
        message.error(res.message);
      }
    });
  };
  reset = val => {
    console.log(val);
    router.push('/home');
  };

  render() {
    const {
      form: { getFieldDecorator, getFieldValue },
      submitting,
    } = this.props;
    const connectionConfigItem = JSON.parse(sessionStorage.getItem('connectionConfigItem')) || {};
    const { schema, schemaList } = this.state;
    const columns = [
      {
        title: '序号',
        dataIndex: 'index',
        key: 'index',
        render(text, record, index) {
          return <span>{index + 1}</span>;
        },
      },
      {
        title: '字段名',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: '字段类型',
        dataIndex: 'type',
        key: 'type',
      },
      {
        title: '字段业务定义',
        key: 'description',
        dataIndex: 'description',
        render: (text, recode, key) => (
          <Input
            onChange={e => {
              e.persist();
              schemaList[key].description = e.target.value;
              this.setState({
                schemaList: schemaList,
              });
            }}
          />
        ),
      },
    ];
    return (
      <div className={styles.main}>
        <Breadcrumb separator=">">
          <Breadcrumb.Item>
            <a href="/home"  className={styles.bread1}>特征列表</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item  className={styles.bread2}>新增特征</Breadcrumb.Item>
        </Breadcrumb>
        <div className={styles.infoList}>新增特征</div>
        <div className={styles.tables}>
        <div className={styles.tablesTop}>
                    <div className={styles.tablesttt}>
                    <div className={styles.tablesTitle}>{schema.name}</div>
                     
                    </div>
                      <Row gutter={16}>
                        <Col span={8}>
                          <Paragraph ellipsis>数据表名：{schema.name}</Paragraph>
                        </Col>
                        <Col span={8}>
                          <Paragraph ellipsis>存储引擎：{schema.type}</Paragraph>
                        </Col>
                        <Col span={8}>
                          <Paragraph ellipsis>创建时间：{schema.createTime?moment(schema.createTime).format("YYYY-MM-DD HH:mm:ss"):""}</Paragraph>
                        </Col>
                        <Col span={8}>
                          <Paragraph ellipsis>创建人：{schema.ownerName}</Paragraph>
                        </Col>
                        <Col span={8}>
                          <Paragraph ellipsis>数据大小：{schema.sizeStr}</Paragraph>
                        </Col>
                        <Col span={8}>
                          <Paragraph ellipsis>更新时间：{schema.updateTime?moment(schema.updateTime).format("YYYY-MM-DD HH:mm:ss"):""}</Paragraph>
                        </Col>
                        <Col span={8}>
                          <Paragraph ellipsis>数据条数：{schema.lineNum}</Paragraph>
                        </Col>
                        <Col span={8}>
                          <Paragraph ellipsis>数据地址：{schema.storageUrl}</Paragraph>
                        </Col>
                        <Col span={8}>
                          <Paragraph ellipsis>数据描述：{schema.description}</Paragraph>
                        </Col>
                        
                      </Row>
                    </div>
          <Table
            columns={columns}
            indentSize={0}
            dataSource={schemaList}
            pagination={false}
            rowKey="name"
            size="middle"
          />
        </div>
        <div className={styles.textRight}>
          <Button onClick={this.reset} className={styles.r20}>
            上一步
          </Button>
          <Button type="primary" onClick={this.hasOk}>
            确定
          </Button>
        </div>
      </div>
    );
  }
}

export default featuresNew;
