import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import searchCss from '../../assets/search.css';
import { Input, Button, Icon, DatePicker, Table ,Select } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { Option } = Select;

@connect(({ other }) => ({
  other,
}))
class BusinessMonitoringQuery extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id ? this.props.id : null,
      name: '',
      subappId: '',
      createdTime:[moment().subtract(7,"days"), moment()],
      current: 1,
      size: 10,
      total: 0,
      resourceColumns: [],
      results: [],
      selectList:[]
    };
  }

  componentDidMount() {
    const { id } = this.state;
    const { dispatch } = this.props;
    dispatch({
      type: 'other/monitorselectScene',
      payload: {size:100},
    }).then(res => {
      if (res.code == 200) {
        var data = res.data;
        this.setState({
          selectList:data.records,
          name:data.records[0].id
        });
        this.search(null,data.records[0].id)
      } else {
      }
    });

  }
  onChange(date, dateString) {
    console.log(date, dateString);
  }
  onChangeTime = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      createdTime: date,
    });
  };

  componentWillUnmount() {}
  reset = () => {
    this.setState(
      {
        name: '',
        subappId: '',
        createdTime: [],
        current: 1,
        size: 10,
        total: 0,
        resourceColumns: [],
        results: [],
      },
      () => {
        this.search();
      },
    );
  };
  search = (num,sceneId) => {
    console.log('search');
    const { dispatch } = this.props;
    const { name, subappId, createdTime, current, size } = this.state;
    console.log(current);
    const search = {
      // appId: name,
      // subAppId:subappId,
      // startTime: createdTime[0] ? moment(createdTime[0]).format('YYYY-MM-DD hh:mm:ss') : '',
      // endTime: createdTime[1] ? moment(createdTime[1]).format('YYYY-MM-DD hh:mm:ss') : '',
      current: num ? num : current,
      size,

      startDate: createdTime[0] ? moment(createdTime[0]).format('YYYY-MM-DD hh:mm:ss') : '',
      endDate: createdTime[1] ? moment(createdTime[1]).format('YYYY-MM-DD hh:mm:ss') : '',
      sceneId:sceneId || name,
    };
    console.log(search);
    // var dataJson = {
    //     sceneId: null,
    //     metricsId: null,
    //     columns: ['book', 'bucketId', 'TA', 'TC'],
    //     values: [
    //       {
    //         bucketId: '1',
    //         TA: 88,
    //         book: '1',
    //         TC: 88,
    //       },
    //       {
    //         TA: 88,
    //         bucketId: '3',
    //         book: '1',
    //       },
    //       {
    //         bucketId: 'bucket1',
    //         book: 'material1',
    //         TC: 1,
    //       },
    //     ],
    //   };
    // this.setState({
    //   current: dataJson.current,
    //   size: dataJson.size,
    //   total: dataJson.total,
    //   resourceColumns: dataJson.columns,
    //   results: dataJson.values,
    // });
    dispatch({
      type: 'other/monitorselect',
      payload: search,
    }).then(res => {
      if (res.code == 200) {
        var data = res.data;
        this.setState({
          current: data.current,
          size: data.size,
          total: data.total,
          resourceColumns: data.columns,
          results: data.values,
        });
      } else {
      }
    });
  };

  render() {
    const { current, size, total, resourceColumns, results ,selectList} = this.state;

    let list = results.map(v => {
      console.log(v);
      if (v.resources) {
        v.resources.forEach(element => {
          var value = [];
          element.value.forEach(val => {
            value.push(val.metricsName + '：' + val.value);
          });
          v[element.name] = value.join('，');
        });
      }
      return v;
    });

    var columns = [
      // {
      //   title: '场景',
      //   dataIndex: 'appId',
      //   align: 'center',
      // },
      // {
      //   title: '子场景',
      //   dataIndex: 'subAppId',
      //   align: 'center',
      // },
    ];
    for (const key of resourceColumns) {
      console.log(key);
      columns.push({
        title: key + '资源',
        dataIndex: key,
        align: 'center',
      });
    }
    // columns.push({
    //   title: '时间',
    //   dataIndex: 'createTime',
    //   align: 'center',
    // });

    const pagination = {
      current: current,
      pageSize: size,
      total: total,
      showQuickJumper: true,
      onChange: (date, dateString) => {
        console.log(date);
        this.setState(
          {
            current: date,
          },
          this.search(date),
        );
      },
    };
     // <Input
            //   className={searchCss.serachIpt}
            //   placeholder="请输入业务指标"
            //   value={this.state.name}
            //   onChange={e => this.setState({ name: e.target.value })}
            //   suffix={<Icon type="search" />}
            // />
    return (
      <div className={styles.bmq}>
        <div className={styles.header}>
          <div className={styles.headerL}>业务监控查询列表</div>
        </div>
        <div className={searchCss.search}>
          <div className={searchCss.searchItem}>
            <span>业务指标：</span>
            <Select  key={selectList.length?selectList[0].id:""} defaultValue={selectList.length?selectList[0].id:""} style={{ width: 220 }} onChange={e => this.setState({ name: e })}>
            {selectList.length?selectList.map((v)=>{
              return <Option value={v.id}>{v.sceneName}</Option>
            }):null}
              
            </Select>
           
            
          </div>
          <div className={searchCss.searchItem}>
            <span>时间选择：</span>
            <RangePicker
              format="YYYY-MM-DD"
              className={styles.picker}
              value={this.state.createdTime}
              onChange={this.onChangeTime}
            />
          </div>
          <div className={searchCss.searchItem + ' ' + styles.searchBtn}>
            <Button className={searchCss.r20} onClick={this.reset}>
              重置
            </Button>
            <Button type="primary" onClick={() => this.search()}>
              搜索
            </Button>
          </div>
        </div>
        <div className={styles.table}>
          <Table columns={columns} dataSource={list} pagination={pagination} size="middle" />
        </div>
      </div>
    );
  }
}

export default BusinessMonitoringQuery;
