import { queryFusionResource ,businessQuery,monitorselect,monitorselectScene} from '../services/api';

export default {
    namespace: 'other',
    state: {
        data: {

        }
    },
    effects: {
        
        *queryFusionResource ({ payload }, { call, put }) {
            const response = yield call(queryFusionResource, payload);
            return response;
        },
        
        *monitorselect ({ payload }, { call, put }) {
            const response = yield call(monitorselect, payload);
            return response;
        },
        
        *monitorselectScene ({ payload }, { call, put }) {
            const response = yield call(monitorselectScene, payload);
            return response;
        },
        
        *businessQuery ({ payload }, { call, put }) {
            const response = yield call(businessQuery, payload);
            return response;
        },
    },

    reducers: {
        responseData (state, action) {
            return {
                ...state,
                ...action.payload,
            };
        }
    },
};
