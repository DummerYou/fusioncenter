import { getTreeFusionFeature ,addViewFusionFeature,saveFusionFeature,featureTableMove,featureGetEnum,getDataSchema,featureMove ,featureRename,registerFusionFeature,getFusionFeature ,featureRemove ,selectFusionFeature} from '../services/api';

export default {
    namespace: 'feature',
    state: {
        data: {

        }
    },
    effects: {
        *featureRename ({ payload }, { call, put }) {
            const response = yield call(featureRename, payload);
            return response;
        },
        *addViewFusionFeature ({ payload }, { call, put }) {
            const response = yield call(addViewFusionFeature, payload);
            return response;
        },
        
        *featureGetEnum ({ payload }, { call, put }) {
            const response = yield call(featureGetEnum, payload);
            return response;
        },
        *featureMove ({ payload }, { call, put }) {
            const response = yield call(featureMove, payload);
            return response;
        },
        *featureTableMove ({ payload }, { call, put }) {
            const response = yield call(featureTableMove, payload);
            return response;
        },
        *featureRemove ({ payload }, { call, put }) {
            const response = yield call(featureRemove, payload);
            return response;
        },
        *getTreeFusionFeature ({ payload }, { call, put }) {
            const response = yield call(getTreeFusionFeature, payload);
            return response;
        },
        *saveFusionFeature ({ payload }, { call, put }) {
            const response = yield call(saveFusionFeature, payload);
            return response;
        },
        *getDataSchema ({ payload }, { call, put }) {
            const response = yield call(getDataSchema, payload);
            return response;
        },
        *registerFusionFeature ({ payload }, { call, put }) {
            const response = yield call(registerFusionFeature, payload);
            return response;
        },
        *getFusionFeature ({ payload }, { call, put }) {
            const response = yield call(getFusionFeature, payload);
            return response;
        },
        *selectFusionFeature ({ payload }, { call, put }) {
            const response = yield call(selectFusionFeature, payload);
            return response;
        },
    },
    
    reducers: {
        responseData (state, action) {
            return {
                ...state,
                ...action.payload,
            };
        }
    },
};
