import { login } from '../services/api';

export default {
    namespace: 'login',
    state: {
        data: {

        }
    },
    effects: {
        *login ({ payload }, { call, put }) {
            const response = yield call(login, payload);
            return response;
        },
    },

    reducers: {
        responseData (state, action) {
            return {
                ...state,
                ...action.payload,
            };
        }
    },
};
