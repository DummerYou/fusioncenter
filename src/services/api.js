
import request from './http';
import request2 from './http2';

// 用户登录
export async function login(params) {
    return request2(`/login/v1/login`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
// 特征搜索接口
export async function getTreeFusionFeature(params) {
    return request(`/features/v1/fusion/feature/getAll`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
// 2.新增特征
export async function saveFusionFeature(params) {
    return request(`/features/v1/fusion/feature/save`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
// 3.获取特征表数据结构
export async function getDataSchema(params) {
    return request(`/features/v1/fusion/feature/getDataSchema`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
// 4.注册特征
export async function registerFusionFeature(params) {
    return request(`/features/v1/fusion/feature/register`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
//5.特征详情
export async function getFusionFeature(params) {
    return request(`/features/v1/fusion/feature/get`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
//5.特征详情
export async function addViewFusionFeature(params) {
    return request(`/features/v1/fusion/feature/addView`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}

// 6.搜索
export async function selectFusionFeature(params) {
    return request(`/features/v1/fusion/feature/search`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
// 6.查询特征列表
export async function getFusionFeature2(params) {
    return request(`/features/v1/fusion/feature/getFusionFeature`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}

// 特征删除接口
export async function featureRemove(params) {
    return request(`/features/v1/fusion/feature/delete`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
// 特征重命名接口
export async function featureRename(params) {
    return request(`/features/v1/fusion/feature/rename`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
// 特征移动接口
export async function featureMove(params) {
    return request(`/features/v1/fusion/feature/move`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
// 特征移动接口
export async function featureTableMove(params) {
    return request(`/features/v1/fusion/feature/tableMove`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}

// 枚举
export async function featureGetEnum(params) {
    return request(`/features/v1/fusion/feature/config/query`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}



// 资源中心
// 资源查询接口

export async function queryFusionResource(params) {
    return request(`/resource/v1/fusion/resource/query`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}

// 业务监控
// 业务监控查询接口

export async function businessQuery(params) {
    return request(`/monitor/v1/fusion/monitor/business/query`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}

export async function monitorselect(params) {
    return request(`/monitor/v1/fusion/monitor/select`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}
export async function monitorselectScene(params) {
    return request(`/monitor/v1/fusion/monitor/selectScene`, {
        method: 'POST',
        body: params,
        postType: 1
    });
}

