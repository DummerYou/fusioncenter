import styles from './index.css';
import Link from 'umi/link';
import zhCN from 'antd/es/locale/zh_CN';
import { ConfigProvider , Menu, Dropdown, Icon } from 'antd';
import { mobileValidator, setStorage, getStorage, rmStorage, decrypt,setCook,getCook } from '../services/utils';
const menu = (
  <Menu>
    <Menu.Item>
      <a rel="noopener noreferrer" href="/">
        退出登录
      </a>
    </Menu.Item>
  </Menu>
);
const getheaderName = (props) => {
  let txt = ""
  console.log(props.location.pathname)
  if(props.location.pathname == "/ResourceQuery"){
    txt = "资源查询"
  }else if(props.location.pathname == "/BusinessMonitoringQuery"){
    txt = "业务监控查询"
  }else{
    txt = '特征中心'
  }
return txt
}
function BasicLayout(props) {
  return (
    <div className={styles.normal}>
      <div className={styles.header}>
        <div className={styles.logo}><div className={styles.loginImg}></div><span>{getheaderName(props)}</span></div>
        <div className={styles.userInfo}>
          {getStorage('userInfo')
            ? (<Dropdown overlay={menu}>
              <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                {getStorage('userInfo').username}<Icon type="down" />
              </a>
            </Dropdown>)
            : (<Link to="/">登录</Link>)}
        </div>
      </div>
      <ConfigProvider locale={zhCN}>
      <div className={styles.conten}>{props.children}</div>
      </ConfigProvider>
    </div>
  );
}

export default BasicLayout;
