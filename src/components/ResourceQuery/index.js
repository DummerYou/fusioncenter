import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import searchCss from '../../assets/search.css';
import { Input, Button, Icon, DatePicker, Table } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

@connect(({ feature }) => ({
  feature,
}))
class ResourceQuery extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id ? this.props.id : null,
      name: '',
      description: '',
      createdTime: [],
      updatedTime: [],
      current: 1,
      size: 10,
    };
  }

  componentDidMount() {
    const { id } = this.state;
  }
  onChange(date, dateString) {
    console.log(date, dateString);
  }
  onChangeTime = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      createdTime: date,
    });
  };

  componentWillUnmount() {}
  reset = () => {
    this.setState({
      name: '',
      description: '',
      createdTime: [],
      updatedTime: [],
      current: '',
      size: '',
    });
  };
  search = () => {
    console.log('search');
    const { dispatch } = this.props;
    const {
      name,
      description,
      createdTime,
      updatedTime,
      current,
      size,
    } = this.state;
    console.log(createdTime);
    const search = {
      name,
      description,
      createdTimeStart: createdTime[0] ? moment(createdTime[0]).format('YYYY-MM-DD h:mm:ss') : '',
      createdTimeEnd: createdTime[1] ? moment(createdTime[1]).format('YYYY-MM-DD h:mm:ss') : '',
      updatedTimeStart: updatedTime[0] ? moment(updatedTime[0]).format('YYYY-MM-DD h:mm:ss') : '',
      updatedTimeEnd: updatedTime[1] ? moment(updatedTime[1]).format('YYYY-MM-DD h:mm:ss') : '',
      current,
      size,
    };
    dispatch({
      type: 'feature/featureSearch',
      payload: search,
    }).then(res => {
      if (res.state) {
        // message.error(res.msg);
      } else {
      }
    });
  };
  render() {
    let list = [];
    for (let i = 0; i < 10; i++) {
      list.push({
        BucketId: 'BucketId',
        TC: 'TC',
        Traffic: 'Traffic',
        Sales: 'Sales',
        TA: 'TA',
        IncTA: 'IncTA',
        SPT: 'SPT',
        IncSPT: 'IncSPT',
        mzl: 1,
        zhl: 2,
        createTiStr: '2021',
      });
    }
    const columns = [
      {
        title: '场景',
        dataIndex: 'BucketId',
        align: 'center',
      },
      {
        title: 'Hadoop资源',
        dataIndex: 'TC',
        align: 'center',
      },
      {
        title: 'K8s资源',
        dataIndex: 'Traffic',
        align: 'center',
      },
      {
        title: 'Flink资源',
        dataIndex: 'Sales',
        align: 'center',
      },
      {
        title: '时间',
        dataIndex: 'createTiStr',
        align: 'center',
        width: 200,
      },
    ];
    const pagination = {
      current: 1,
      pageSize: 20,
      total: 99,
      showQuickJumper:true,
      onChange : (date, dateString) => {
        console.log(date)
      }
  };
    return (
      <div className={styles.bmq}>
        <div className={searchCss.search}>
          <div className={searchCss.searchs}>
            <div className={searchCss.searchItem}>
              <span>场景名称：</span>
              <Input
                className={searchCss.serachIpt}
                placeholder="请输入场景名称"
                value={this.state.name}
                onChange={e => this.setState({ name: e.target.value })}
                suffix={<Icon type="search" />}
              />
            </div>
          </div>
          <div className={searchCss.searchs}>
            <div className={searchCss.searchItem}>
              <span>时间选择：</span>
              <RangePicker value={this.state.createdTime} onChange={this.onChangeTime} />
            </div>
          </div>
          <div className={searchCss.searchs}>
            <div className={searchCss.searchItem}>
              <Button className={searchCss.r20} onClick={this.reset}>
                重置
              </Button>
              <Button type="primary" onClick={this.search}>
                搜索
              </Button>
            </div>
          </div>
        </div>
        <div className={styles.table}>
          <Table columns={columns} dataSource={list}  pagination={pagination} />
        </div>
      </div>
    );
  }
}
export default ResourceQuery;
