import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import searchCss from '../../assets/search.css';
import { Input, Button, Icon, DatePicker, Table, Modal, Form, Select } from 'antd';
import moment from 'moment';
import router from 'umi/router';
import { connect } from 'dva';
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { Column, ColumnGroup } = Table;
const { confirm } = Modal;
const { Option } = Select;

@connect(({ feature }) => ({
  feature,
}))
@Form.create()
class FeatureCenter extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      loading: false,
      id: this.props.id ? this.props.id : null,
      name: '',
      description: '',
      createdTime: [],
      updatedTime: [],
      current: 1,
      size: 10,
      list: [],
    };
  }
  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    const { dispatch } = this.props;
    // this.setState({
    //   visible: false,
    // });
    this.props.form.validateFields(['title', 'select', 'description2'], (err, values) => {
      console.log(values);
      if (!err) {
        dispatch({
          type: 'feature/saveFusionFeature',
          payload: values,
        }).then(res => {
          console.log(res);
          if (res.code == 200) {
            
          } else {
          }
        });
      }
    });
    // router.push('/featuresNew');
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  componentDidMount() {
    this.search();
  }
  onChange(date, dateString) {
    console.log(date, dateString);
  }
  onChangeTime1 = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      createdTime: date,
    });
  };
  onChangeTime2 = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      updatedTime: date,
    });
  };
  deleteClick(id) {
    confirm({
      title: '您确定要删除该特征数据吗?' + id,
      okText: '确定',
      okType: 'danger',
      cancelText: '取消',
      onOk() {
        console.log('OK');
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  reset = () => {
    this.setState({
      name: '',
      description: '',
      createdTime: [],
      updatedTime: [],
      current: '',
      size: '',
    });
    this.search();
  };
  search = () => {
    const { dispatch } = this.props;
    const { name, description, createdTime, updatedTime, current, size } = this.state;
    const search = {
      name,
      description,
      createdTimeStart: createdTime[0] ? moment(createdTime[0]).format('YYYY-MM-DD h:mm:ss') : '',
      createdTimeEnd: createdTime[1] ? moment(createdTime[1]).format('YYYY-MM-DD h:mm:ss') : '',
      updatedTimeStart: updatedTime[0] ? moment(updatedTime[0]).format('YYYY-MM-DD h:mm:ss') : '',
      updatedTimeEnd: updatedTime[1] ? moment(updatedTime[1]).format('YYYY-MM-DD h:mm:ss') : '',
      current,
      size,
    };
    dispatch({
      type: 'feature/getTreeFusionFeature',
      payload: search,
    }).then(res => {
      if (res.code == 200) {
        console.log(res);
        // message.error(res.msg);
        this.setState({
          list: res.data,
        });
        console.log(this.state.list);
      } else {
      }
    });
  };

  componentWillUnmount() {}

  render() {
    const { visible, loading, list } = this.state;
    const { onCancel, onCreate, form } = this.props;
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };

    console.log(list);
    const columns = [
      {
        title: '特征数据',
        dataIndex: 'name',
        align: 'left',
        width: 300,
        render: (text, record) => (
          <span
            onClick={() => {
              console.log(record);
            }}
          >
            {record.name}
          </span>
        ),
      },
      {
        title: '描述',
        dataIndex: 'description',
        align: 'center',
      },
      {
        title: '存储引擎',
        dataIndex: 'storageType',
        align: 'center',
        render: (text, record) => <span>{text || '- -'}</span>,
      },
      {
        title: '创建人',
        dataIndex: 'createdBy',
        align: 'center',
      },
      {
        title: '创建时间',
        dataIndex: 'createdTime',
        align: 'center',
        width: 200,
      },
      {
        title: '更新时间',
        dataIndex: 'updatedTime',
        align: 'center',
        width: 200,
      },

      {
        title: '操作',
        key: 'action',
        render: (text, record) => (
          <span>
            <Button
              className={styles.tabBtn}
              type="link"
              onClick={() => {
                router.push('/featureCenterList?id=456');
              }}
              disabled={record.children && record.children.length && !record.children[0].children}
            >
              详情
            </Button>
            <Button
              className={styles.tabBtn}
              type="link"
              onClick={() => {
                console.log(record);
              }}
              disabled={record.children && record.children.length}
            >
              重命名
            </Button>
            <Button className={styles.tabBtn} type="link">
              移动到
            </Button>
            <Button
              className={styles.tabBtn}
              type="link"
              onClick={() => this.deleteClick(record.id)}
              disabled={record.children && record.children.length}
            >
              删除
            </Button>
          </span>
        ),
        align: 'center',
        width: 200,
      },
    ];
    const pagination = {
      current: 1,
      pageSize: 20,
      total: 99,
      showQuickJumper: true,
      onChange: (date, dateString) => {
        console.log(date);
      },
    };
    return (
      <div className={styles.bmq}>
        <div className={styles.header}>
          <div className={styles.headerL}>特征列表</div>
          <Button icon="plus" onClick={this.showModal}>
            新增特征
          </Button>
        </div>
        <div className={searchCss.search}>
          <div className={searchCss.searchs}>
            <div className={searchCss.searchItem}>
              <div className={searchCss.searchItemTwo}>
                <span>文件名称：</span>
                <Input
                  className={searchCss.serachIpt}
                  value={this.state.name}
                  onChange={e => this.setState({ name: e.target.value })}
                  placeholder="请输入业务指标"
                  suffix={<Icon type="search" />}
                />
              </div>
              <div>
                <span>创建时间：</span>
                <RangePicker value={this.state.createdTime} onChange={this.onChangeTime1} />
              </div>
            </div>
          </div>
          <div className={searchCss.searchs}>
            <div className={searchCss.searchItem}>
              <div className={searchCss.searchItemTwo}>
                <span>描述：</span>
                <Input
                  className={searchCss.serachIpt}
                  placeholder="请输入描述"
                  value={this.state.description}
                  onChange={e => this.setState({ description: e.target.value })}
                  suffix={<Icon type="search" />}
                />
              </div>
              <div>
                <span>更新时间：</span>
                <RangePicker value={this.state.updatedTime} onChange={this.onChangeTime2} />
              </div>
            </div>
          </div>
          <div className={searchCss.searchs}>
            <div className={searchCss.searchItem}>
              <div className={searchCss.searchItemTwo}></div>
              <div>
                <Button className={searchCss.r20} onClick={this.reset}>
                  重置
                </Button>
                <Button type="primary" onClick={this.search}>
                  搜索
                </Button>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.table}>
          <Table columns={columns} dataSource={list} rowKey={'id'} pagination={pagination}></Table>
        </div>
        <Modal
          title="新增特征"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              取消
            </Button>,
            <Button key="submit" type="primary" loading={loading} onClick={this.handleOk}>
              下一步
            </Button>,
          ]}
        >
          <Form {...formItemLayout}>
            <Form.Item label="特征路径">
              {getFieldDecorator('title')(<Button type="link">选择路径</Button>)}
            </Form.Item>
            <Form.Item label="数据类型">
              {getFieldDecorator('select', {
                initialValue: '0',
              })(
                <Select>
                  <Option value="0">HDFS</Option>
                  <Option value="1">RtiDB</Option>
                  <Option value="2">Hive</Option>
                  <Option value="3">Es</Option>
                </Select>,
              )}
            </Form.Item>
            <Form.Item label="数据路径">{getFieldDecorator('description2')(<Input />)}</Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
}

export default FeatureCenter;
