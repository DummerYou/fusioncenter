let dataJson2 = {
  "code": 200,
  "message": "请求成功",
  "data": {
    "records": [
      {
        "current": 0,
        "size": 0,
        "id": 22,
        "createdBy": "SYSTEM",
        "updatedBy": "SYSTEM",
        "createdTime": "2021-06-21 16:31:28",
        "updatedTime": "2021-08-06 13:39:49",
        "key": null,
        "levelType": 1,
        "isExpand": 1,
        "sortField": null,
        "parentId": 2,
        "name": "测试名称22",
        "type": 1,
        "level": null,
        "path": "特征名称/KFC/特征中心/测试名称22",
        "description": "测试描述1234",
        "occupyStorage": 1,
        "occupyStorageStr": "1KB",
        "subset": [
          {
            "current": 0,
            "size": 0,
            "id": 4,
            "createdBy": "SYSTEM",
            "updatedBy": "SYSTEM",
            "createdTime": "2021-07-28 15:52:46",
            "updatedTime": "2021-08-03 15:30:14",
            "key": null,
            "levelType": 2,
            "isExpand": 1,
            "sortField": null,
            "featureId": 22,
            "name": "视图1",
            "subset": [
              {
                "current": 0,
                "size": 0,
                "id": 1,
                "createdBy": "SYSTEM",
                "updatedBy": "SYSTEM",
                "createdTime": "2021-05-27 16:49:39",
                "updatedTime": "2021-08-06 13:36:35",
                "key": null,
                "levelType": 3,
                "isExpand": 1,
                "sortField": null,
                "viewId": 4,
                "name": "Customer.table",
                "storageEngine": 0,
                "storageEngineStr": "HDFS",
                "storageUrl": null,
                "connectionType": 1,
                "connectionConfig": "{\"url\":\"http://172.27.96.215:40121\",\"aiosPrn\":\"3cf7e5cd-02d0-446b-a820-fc7160ad0cf6/data_group_shand.table-group/banking_data1.table\",\"aiosWorkspaceId\":17}",
                "recordNum": 41187,
                "dataSize": 1,
                "dataSizeStr": "1KB",
                "description": "表说明",
                "subset": [
                  {
                    "current": 0,
                    "size": 0,
                    "id": 1,
                    "createdBy": "SYSTEM",
                    "updatedBy": "SYSTEM",
                    "createdTime": "2021-07-09 14:46:47",
                    "updatedTime": "2021-08-06 16:56:16",
                    "key": null,
                    "levelType": 4,
                    "isExpand": 0,
                    "sortField": null,
                    "fusionDataId": 1,
                    "name": "job",
                    "type": "String",
                    "description": null
                  },
                  {
                    "current": 0,
                    "size": 0,
                    "id": 4,
                    "createdBy": "SYSTEM",
                    "updatedBy": "SYSTEM",
                    "createdTime": "2021-07-14 16:26:18",
                    "updatedTime": "2021-08-02 09:47:15",
                    "key": null,
                    "levelType": 4,
                    "isExpand": 0,
                    "sortField": null,
                    "fusionDataId": 1,
                    "name": "job",
                    "type": "String",
                    "description": ""
                  }
                ]
              }
            ]
          }
        ],
        "children": null
      }
    ],
    "total": 7,
    "size": 10,
    "current": 1,
    "orders": [],
    "optimizeCountSql": true,
    "hitCount": false,
    "countId": null,
    "maxLimit": null,
    "searchCount": true,
    "pages": 1
  }
}
export default dataJson2