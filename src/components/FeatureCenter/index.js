import React, { PureComponent, Fragment } from 'react';
import styles from './index.css';
import searchCss from '../../assets/search.css';
import { Input, Button, Icon, DatePicker, Table, Modal, Form, Select, message } from 'antd';
import { setCook ,getStorage} from '../../services/utils';
import moment from 'moment';
import router from 'umi/router';
import { connect } from 'dva';
import dataJson2 from './aa2.js';
import dataJsona from './aa.js';
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { Column, ColumnGroup } = Table;
const { confirm } = Modal;
const { Option } = Select;

@connect(({ feature }) => ({
  feature,
}))
@Form.create()
class FeatureCenter extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      visible1: false,
      visible1State: 0,
      visible1Id: null,
      visible1Item:null,
      visibleTables:false,
      stClickState:false,
      loading: false,
      id: this.props.id ? this.props.id : null,
      name: '',
      description: '',
      queryView: '',
      queryTable: '',
      queryField: '',
      createdTime: [],
      updatedTime: [],
      current: 1,
      size: 10,
      list: [],
      listOld: [],
      listFolder: [],
      listFolderOld: [],
      selectedRowKeys: [],
      modifyText: '',
      listFolderText1: '',
      listFolderText2: '',
      listFolderId: '',
      listFoldererr: '',
      listFolderState: false,
      getEnum: [],
      featureList: {},
      connectionConfig:[],
      expandedRowKeys:[],
    };
  }
  showModal = () => {
    this.getFusionFeature();
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    const { featureList ,getEnum,connectionConfig} = this.state;
    const { dispatch } = this.props;
    // console.log(featureList);
    if (!featureList.id) {
      message.error('请选择特征路径');
      return;
    }

    var list = ['select'];
    connectionConfig.forEach(v=>{
      list.push(v.field)
    })
    // console.log(list);
    this.props.form.validateFields(list, (err, values) => {
      // console.log(values);
      if (!err) {
        const connect = this.deepClone(values)
        delete connect.select
        // console.log(JSON.stringify(connect));
        // console.log(values);
        // var connectionConfig = { aiosUrl: '', aiosPrn: '', aiosWorkspaceId: 17 };
        // var connectionConfig = {
        //   aiosUrl: 'http://172.27.96.215:40121/telamon/v1/tables',
        //   aiosPrn:
        //     '3cf7e5cd-02d0-446b-a820-fc7160ad0cf6/data_group_shand.table-group/banking_data1.table',
        //   aiosWorkspaceId: 1,
        // };
        // sessionStorage.setItem('connectionConfig', JSON.stringify(connectionConfig));value name
        
        var storageEngineTxt = getEnum.find((v)=>{return v.id == values.select})
        var conList = JSON.parse(storageEngineTxt.connectionConfig).map((v)=>{
          v.value = connect[v.field] || "";
          return v
        })
        console.log(conList)
        
        console.log(connect)
        const connectionConfigItem = {
          featureId: featureList.id,
          storageEngine: values.select,
          storageEngineTxt:storageEngineTxt?storageEngineTxt.connectionType : "",
          connectionConfig:conList,
          storageEngine1:storageEngineTxt.storageEngine
        };
        console.log(storageEngineTxt);
        const data = {
          id:storageEngineTxt.id,
          connectionConfig: JSON.stringify(connect),
        };
        dispatch({
          type: 'feature/getDataSchema',
          payload: data,
        }).then(res => {
          console.log(res);
          if (res.code === 200) {
            
            sessionStorage.setItem('connectionConfig', res.data.connectionConfig); //connect
            sessionStorage.setItem('connectionConfigSchema', JSON.stringify(res.data));
            sessionStorage.setItem('connectionConfigItem', JSON.stringify(connectionConfigItem));
            router.push('/featuresNew');
          } else {
            message.error(res.message || "错误");
          }
        });
        
      }
    });
  };

  handleCancel = e => {
    console.log(e);
    this.props.form.resetFields();
    this.setState({
      visible: false,
      featureList: {},
    });
  };
  showModal1 = () => {
    console.log('object');
    this.setState({
      visible1: true,
      visible1State: 0,
      selectedRowKeys: [],
    });
  };
  showModalmobile = item => {
    console.log(item);
    
    this.getFusionFeature(1);
    var visibleTables = false
    if(item.tables){
      visibleTables = true
    }
    this.setState({
      visible1: true,
      visible1State: 1,
      visible1Id: item.id,
      visible1Item: item,
      visibleTables:visibleTables,
      selectedRowKeys: [],
    });
  };
  getCheckboxProps = (record) => {
    const {
      visible1State,
      visible1Id,
      listFolder,
      listFolderOld,
      listFolderText1,
      listFolderText2,
      visibleTables
    } = this.state;
    if(visible1State != 0){
      return ({
        disabled: false
      })
    }
    console.log(record)
    console.log(visible1State);
    console.log(visible1State == 0 && !record.children);
    var lists = this.deepClone(listFolderOld);
    var listOrder = this.getPathByKey(record.id, 'id', lists) || [];
    console.log(listOrder);
    if (listOrder.length >= 1) {
      var text = 'lists[' + listOrder.join(']["children"][') + ']';
      console.log(text);
      console.log(eval(text));
    }
    var state = eval(text).children && eval(text).children.length
    console.log(state);
    return ({
         disabled: visible1State == 0 && state
    })
}
  handleOk1 = e => {
    const {
      listOld,
      listFolder,
      listFolderState,
      listFolderId,
      listFolderText1,
      listFolderText2,
      stClickState,
    } = this.state;
    const { dispatch } = this.props;
    if (listFolderState) {
      console.log('object');
      console.log(listFolder);
      console.log(listFolderId);
      console.log(listFolderText1);
      console.log(listFolderText2);
      if (!listFolderText1) {
        let txtFo = ""
        if(stClickState){
          txtFo = '请输入视图名称'
        }else{
          txtFo ='请输入特征表'
        }
        this.setState({
          listFoldererr: txtFo,
        });
        this.forceUpdate();
        return;
      }
      if(stClickState){
        dispatch({
          type: 'feature/addViewFusionFeature',
          payload: {  name: listFolderText1, featureId: listFolderId,operator:getStorage('userInfo')?getStorage('userInfo').username:'', },
        }).then(res => {
          if (res.code == 200) {
            console.log(res);
            // message.error(res.msg);
            this.setState({
              visible1: false,
              listFolderState: false,
              listFolderId: '',
              listFolderText1: '',
              listFolderText2: '',
            });
            this.search();
            this.forceUpdate();
            this.getFusionFeature();
          } else {
            this.setState({
              listFoldererr: res.message,
            });
            this.forceUpdate();
          }
        });
      }else{
        dispatch({
          type: 'feature/saveFusionFeature',
          payload: { description: listFolderText2, name: listFolderText1, parentId: listFolderId },
        }).then(res => {
          if (res.code == 200) {
            console.log(res);
            // message.error(res.msg);
            this.setState({
              visible1: false,
              listFolderState: false,
              listFolderId: '',
              listFolderText1: '',
              listFolderText2: '',
            });
            this.search();
            this.forceUpdate();
            this.getFusionFeature();
          } else {
            this.setState({
              listFoldererr: res.message,
            });
            this.forceUpdate();
          }
        });
      }
      
    } else {
      this.setFolderType();
    }
  };
  setFolderType = () => {
    const {
      selectedRowKeys,
      visible1State,
      visible1Id,
      visible1Item,
      listFolder,
      listFolderOld,
      listFolderText1,
      listFolderText2,
      visibleTables
    } = this.state;
    const { dispatch } = this.props;
    const _this = this;

    if (visible1State == 0) {
      // 新增特征
      console.log('新增特征');
      console.log(listFolder);
      console.log(selectedRowKeys[0]);
      // var fileList = listFolder.filter(item => {
      //   return item.id == selectedRowKeys[0];
      // });

      var lists = this.deepClone(listFolderOld);
      var listOrder = this.getPathByKey(selectedRowKeys[0], 'keyId', lists) || [];
      console.log(listOrder);
      if (listOrder.length >= 1) {
        var text = 'lists[' + listOrder.join(']["children"][') + ']';
        console.log(text);
        console.log(eval(text));
        if(eval(text).levelType !== 2){
          message.error("请选择视图进行新增");
          return
        }
        this.setState({
          featureList: eval(text) || {},
          visible1: false,
        });
        this.forceUpdate();
      }

      // listFolderText1:"",
      // listFolderText2:"",
      // this.setState({
      //   visible1: false,
      // });
      // router.push('/featuresNew');
    } else if (visible1State == 1) {
      // 移动特征
      // console.log(selectedRowKeys[0]);
      // console.log(visibleTables);
      // var lists = this.deepClone(listFolderOld);
      // var listOrder = this.getPathByKey(selectedRowKeys[0], 'keyId', lists) || [];
      // console.log(listOrder);
      // if (listOrder.length >= 1) {
      //   var text = 'lists[' + listOrder.join(']["children"][') + ']';
      //   console.log(text);
      //   console.log(eval(text));
      // }
      if (selectedRowKeys[0]) {
        dispatch({
          type: 'feature/featureMove',
          payload: { id: visible1Id,levelType:visible1Item.levelType, parentId: selectedRowKeys[0].slice(2) },
        }).then(res => {
          if (res.code == 200) {
            console.log(res);
            // message.error(res.msg);
            this.setState({
              visible1: false,
            });
            _this.search();
          } else {
            message.error(res.message);
          }
        });
        // if(visibleTables){
         
        // }else{
        //   dispatch({
        //     type: 'feature/featureTableMove',
        //     payload: { dataId: visible1Id, featureId: selectedRowKeys[0].slice(2) },
        //   }).then(res => {
        //     if (res.code == 200) {
        //       console.log(res);
        //       // message.error(res.msg);
        //       this.setState({
        //         visible1: false,
        //       });
        //       _this.search();
        //     } else {
        //       message.error(res.message);
        //     }
        //   });
        // }
      }
    }
  };
  mobileFolder = () => {};
  addFolder2 = () => {
    this.addFolder(2)
  };
  addFolder = (num) => {
    const { selectedRowKeys,visible1State, listOld, listFolder, listFolderOld, listFolderState } = this.state;
    
    console.log(selectedRowKeys[0]);

    if (listFolderState) {
      return;
    }
    if(num == 2){
      this.setState({
        stClickState: true,
      });
    }else{
      this.setState({
        stClickState: false,
      });
    }

    var lists = this.deepClone(listFolderOld);
    var dataItem = {
      createdTime: moment().format('YYYY-MM-DD h:mm:ss'),
      description: '',
      name: '',
      editable: true,
      id: 999999,
    };
    var listOrder = this.getPathByKey(selectedRowKeys[0], 'keyId', lists) || [];
    console.log(listOrder);
    
    if (listOrder.length >= 1) {
      var text = 'lists[' + listOrder.join(']["children"][') + ']';
      console.log(text);
      console.log(eval(text));
      if(num == 2){
        if(eval(text).levelType != 1){
          message.error("请选择目录进行新建视图");
          return;
        }
        if(eval(text).children && eval(text).children.length && eval(text).children[0].levelType == 1){
          message.error("请选择末级节点进行新建视图");
          return;
        }
        // levelType == 2
      }
      if (eval(text).children) {
        eval(text).children.push(dataItem);
      } else {
        eval(text).children = [dataItem];
      }
    } else {
      if(num == 2){
          message.error("请选择末级节点进行新建视图");
          return;
      }
      lists.push(dataItem);
    }
    console.log(lists);

    this.setState({
      listFolderId: selectedRowKeys[0].slice(2) || 0,
      listFolder: lists,
      selectedRowKeys: [999999],
      listFolderState: true,
      listFoldererr: "",
    });

    console.log('789');
  };

  handleCancel1 = e => {
    const { listFolderOld } = this.state;
    console.log(e);
    this.setState({
      visible1: false,
      listFolder: listFolderOld,
      selectedRowKeys: [],
      listFolderState: false,
      listFolderText1: '',
      listFolderText2: '',
      listFolderId: '',
    });
  };
  componentDidMount() {
    const { dispatch } = this.props;
    this.search();
    // this.setState({
    //   getEnum: dataJsona.data,
    //   connectionConfig: dataJsona.data[0]?JSON.parse(dataJsona.data[0].connectionConfig):[],
    // });

    
    // this.getFusionFeature();
  }
  getFusionFeature = val => {
    const { dispatch } = this.props;
    // console.log(dataJson2);
    // this.setState({
    //   listFolder: dataJson2.data.records,
    //   listFolderOld: dataJson2.data.records,
    // });
    console.log(val);
    if(val != 1){
      dispatch({
        type: 'feature/featureGetEnum',
        // payload: { name: 'storageEngineEnum' },
      }).then(res => {
        console.log(res);
        if (res.data && res.data.length) {
          console.log(res.data);
          // message.error(res.msg);
          this.setState({
            getEnum: res.data,
            connectionConfig: res.data[0]?JSON.parse(res.data[0].connectionConfig):[],
          });
        } else {
        }
      });
    }
    // console.log(dataJsona);
    // this.setState({
    //   listFolder: dataJsona.data,
    //   listFolderOld: dataJsona.data,
    // });
    dispatch({
      type: 'feature/getTreeFusionFeature',
    }).then(res => {
      if (res.code == 200) {
        console.log(res);
        // message.error(res.msg);
        this.setState({
          listFolder: this.eachKeyId(res.data),
          listFolderOld: this.eachKeyId(res.data),
        });
        console.log(this.state.list);
      } else {
      }
    });
  };
  onChange(date, dateString) {
    console.log(date, dateString);
  }
  onChangeTime1 = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      createdTime: date,
    });
  };
  onChangeTime2 = (date, dateString) => {
    console.log(date, dateString);
    this.setState({
      updatedTime: date,
    });
  };

  /*
   * 设置表格单选状态
   * **/
  getCheckboxProps3 = record => {
    return {
      disabled: record.child === true, // 配置默认禁用的选项 含有孩子节点的字段名称为child =true
    };
  };
  /*
   * 单，多选类型时候使用
   * */
  onChange3 = (selectedRowKeys, selectedRows) => {
    const { listFolderState } = this.state;
    if(!listFolderState){
      this.setState({
        selectedRowKeys: selectedRowKeys,
      });
    }
    
  };

  deleteClick(record) {
    const { dispatch } = this.props;
    const _this = this;
    confirm({
      title: '您确定要删除该特征表吗?',
      okText: '确定',
      okType: 'danger',
      cancelText: '取消',
      onOk() {
        console.log('OK');

        dispatch({
          type: 'feature/featureRemove',
          payload: { id: record.id,levelType:record.levelType },
        }).then(res => {
          if (res.code == 200) {
            console.log(res);
            _this.search();
          } else {
            message.error(res.message);
          }
        });
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  reset = () => {
    this.setState(
      {
        name: '',
        description: '',
        queryView: '',
        queryTable: '',
        queryField: '',
        createdTime: [],
        updatedTime: [],
        current: 1,
        size: 10,
        total: 0,
      },
      () => {
        this.search();
      },
    );
  };
  nameBlur = (record, e, key) => {
    e.persist();
    console.log(record, e, key);
    const { list, listOld, modifyText } = this.state;
    var lists = this.deepClone(listOld);
    const { dispatch } = this.props;
    
    var listOrder = this.getPathByKey(record.keyId, 'keyId', lists,'subset') || [];
    console.log(listOrder);
    var text = []
    if (listOrder.length >= 1) {
      text = 'lists[' + listOrder.join(']["subset"][') + ']';
      console.log(text);
      console.log(eval(text));
      
      if (!modifyText || modifyText === eval(text).name) {
        eval(text).editable = false;
        this.setState({
          list: lists,
          listOld: lists,
        });
        this.forceUpdate();
        return;
      }
    }




    

    dispatch({
      type: 'feature/featureRename',
      payload: {
        id: record.id,
        levelType:record.levelType,
        name: e.target.value,
      },
    }).then(res => {
      if (res.code == 200) {
        console.log(record.keyId, lists);
        var listOrder = this.getPathByKey(record.keyId, 'keyId', lists,'subset') || [];
        console.log(listOrder);
        if (listOrder.length >= 1) {
          var text = 'lists[' + listOrder.join(']["subset"][') + ']';
          console.log(text);
          console.log(eval(text));
          eval(text).editable = false;
          eval(text).name = modifyText;
         
        }
        this.setState({
          list: lists,
          listOld: lists,
        });
        this.forceUpdate();
      } else {
        // if (listOrder.length >= 1) {
        //   var text = 'list[' + listOrder.join(']["children"][') + ']';
        //   eval(text).errText = res.message ;
        // }
        eval(text).errText = res.message;
        this.setState({
          list: list,
        });
        this.forceUpdate();
      }
    });
  };
  setListArr = (record, e, key) => {
    const { list, listOld } = this.state;
    var lists = this.deepClone(listOld);
    console.log(record);
    var listOrder = this.getPathByKey(record.keyId, 'keyId', lists,'subset') || [];
    console.log(listOrder);
    if (listOrder.length >= 1) {
      var text = 'lists[' + listOrder.join(']["subset"][') + ']';
      eval(text).editable = true;
    }
    // lists[key].editable = true;
    this.setState({
      list: lists,
      modifyText: record.name,
    });
    this.forceUpdate();
  };
  getPathByKey = (value, key, arr,children='children') => {
    let temppath = [];
    try {
      function getNodePath(node, index) {
        temppath.push(index);
        if (node[key] === value) {
          throw 'GOT IT!';
        }
        if (node[children] && node[children].length > 0) {
          for (var i = 0; i < node[children].length; i++) {
            getNodePath(node[children][i], i);
          }
          temppath.pop();
        } else {
          temppath.pop();
        }
      }
      for (let i = 0; i < arr.length; i++) {
        getNodePath(arr[i], i);
      }
    } catch (e) {
      return temppath;
    }
  };
  // 定义一个深拷贝函数  接收目标target参数
  deepClone = target => {
    // 定义一个变量
    let result;
    // 如果当前需要深拷贝的是一个对象的话
    if (typeof target === 'object') {
      // 如果是一个数组的话
      if (Array.isArray(target)) {
        result = []; // 将result赋值为一个数组，并且执行遍历
        for (let i in target) {
          // 递归克隆数组中的每一项
          result.push(this.deepClone(target[i]));
        }
        // 判断如果当前的值是null的话；直接赋值为null
      } else if (target === null) {
        result = null;
        // 判断如果当前的值是一个RegExp对象的话，直接赋值
      } else if (target.constructor === RegExp) {
        result = target;
      } else {
        // 否则是普通对象，直接for in循环，递归赋值对象的所有值
        result = {};
        for (let i in target) {
          result[i] = this.deepClone(target[i]);
        }
      }
      // 如果不是对象的话，就是基本数据类型，那么直接赋值
    } else {
      result = target;
    }
    // 返回最终结果
    return result;
  };
  /**
     *遍历替换属性名
     * @param list 
     * @return [] 
     */
     eachKeyId = (list,children = "children") => {
       let _this = this
      if (Array.isArray(list) && list.length !== 0) {
        list = list.map(v => {
            v.keyId = "1" + (v.levelType || "0") + v.id
            if (v[children]) {
                v[children] = _this.eachKeyId(v[children],children)
            }
            return v
        })
        return list
    } else {
        return []
    }
   };
   
  /**
     *遍历修改展开项
     * @param list 
     * @return [] 
     */
    eachKeyId2 = (list,children = "children") => {
      let _this = this
     if (Array.isArray(list) && list.length !== 0) {
      let data = []
       list = list.forEach(v => {
           if(v.isExpand){
                data.push("1" + (v.levelType || "0") + v.id)
            }
           if (v[children]) {
               data = data.concat(_this.eachKeyId2(v[children],children))
           }
       })
       return data
   } else {
       return []
   }
  };
  search = num => {
    const { dispatch } = this.props;
    
    const { name, description,queryView,queryTable,queryField, createdTime, updatedTime, current, size } = this.state;
    const search = {
      name,
      description,queryView,queryTable,queryField,
      createdTimeStart: createdTime[0] ? moment(createdTime[0]).format('YYYY-MM-DD h:mm:ss') : '',
      createdTimeEnd: createdTime[1] ? moment(createdTime[1]).format('YYYY-MM-DD h:mm:ss') : '',
      updatedTimeStart: updatedTime[0] ? moment(updatedTime[0]).format('YYYY-MM-DD h:mm:ss') : '',
      updatedTimeEnd: updatedTime[1] ? moment(updatedTime[1]).format('YYYY-MM-DD h:mm:ss') : '',
      current: num ? num : current,
      size,
    };

    
    // var data = dataJson2.data;
    // var records = data.records;
    // console.log(data);
    // this.setState({
    //   list: records,
    //   listOld: records,
    //   current: data.current,
    //   size: data.size,
    //   total: data.total,
    // });

    dispatch({
      type: 'feature/selectFusionFeature',
      payload: search,
    }).then(res => {
      if (res.code == 200) {
        console.log(res);
        // message.error(res.msg);
        var data = res.data;
        var records = this.eachKeyId(data.records,'subset');
        console.log();
        let kkkk = this.eachKeyId2(data.records,'subset')
        this.setState({
          list: records,
          listOld: records,
          current: data.current,
          size: data.size,
          total: data.total,
          expandedRowKeys:kkkk
        });
        console.log(this.state.list);
      } else {
        // message.error(res.msg);
      }
    });
  };
  selectChange = (e) => {
    const { getEnum } = this.state;
    var storageEngineTxt = getEnum.find((v)=>{return v.id == e})
    console.log(e);
    console.log(storageEngineTxt);
    console.log(JSON.parse(storageEngineTxt.connectionConfig));

    this.setState({
      connectionConfig: JSON.parse(storageEngineTxt.connectionConfig),
    });
  }
  getStorageEngine = record => {
    const { getEnum } = this.state;
    const brr = getEnum.filter(item => {
      return item.value == record.storageEngine;
    });
    if (brr.length) {
      return brr[0].name;
    } else {
      return '- -';
    }
  };
  expandedRowClick = (expanded, record) =>{
    console.log(expanded);
    console.log(record);
    const {
      expandedRowKeys,
    } = this.state;
    
    let exKey = expandedRowKeys
    if(expanded){
      exKey.push(record.keyId)
    }else{
      var index = exKey.indexOf(record.keyId); 
      if (index > -1) { 
        exKey.splice(index, 1);}
    }
    console.log(exKey);
    this.setState({
      expandedRowKeys: exKey,
    });
    this.forceUpdate();
  }
  componentWillUnmount() {}

  render() {
    const {
      current,
      size,
      total,
      visible,
      loading,
      list,
      listFolder,
      getEnum,
      featureList,
      listFolderState,
      listFoldererr,
      visible1State,
      connectionConfig,
      stClickState,
      expandedRowKeys,
    } = this.state;
    const { onCancel, onCreate, form } = this.props;
    const { getFieldDecorator } = form;
    setCook('headerName', '特征中心');
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };

    const columns1 = [
      {
        title: '特征表',
        dataIndex: 'name',
        align: 'left',
        width: 300,
        render: (text, record, key) =>
          record.editable ? (
            <div className={styles.iptSet}>
              {record.levelType==1?<Icon type="folder" />:
              record.levelType==2?<Icon type="file" />:
              record.levelType==3?<Icon type="table" />:
              record.levelType==4?<Icon type="dot-chart" />:null}
              <Input
                value={this.state.listFolderText1}
                onChange={e => {
                  this.setState({
                    listFolderText1: e.target.value,
                    listFoldererr: '',
                  });
                }}
              />
              <span className={styles.iptSetSpan}>{listFoldererr}</span>
            </div>
          ) : (
            <span>{record.levelType==1?<Icon type="folder" />:
            record.levelType==2?<Icon type="file" />:
            record.levelType==3?<Icon type="table" />:
            record.levelType==4?<Icon type="dot-chart" />:null}&nbsp;&nbsp;{record.name}</span>
          ),
      },
      {
        title: '描述',
        dataIndex: 'description',
        align: 'center',
        width: 200,
        render: (text, record, key) =>
          (record.editable && !stClickState) ? (
            <div className={styles.iptSet}>
              <Input
                value={this.state.listFolderText2}
                onChange={e => {
                  this.setState({
                    listFolderText2: e.target.value,
                  });
                }}
              />
            </div>
          ) : (
            <span>{record.description}</span>
          ),
      },
      {
        title: '总占用存储',
        dataIndex: 'occupyStorageStr',
        align: 'center',
        width: 100,
        render: (text, record, key) =>
          record.occupyStorageStr ? <span>{record.occupyStorageStr}</span> : <span>--</span>,
      },
      {
        title: '创建时间',
        dataIndex: 'createdTime',
        align: 'center',
        width: 200,
      },
    ];
    const columns = [
      {
        title:"",
        dataIndex: 'levelType',
        align: 'left',
        width: 100,
        render: (text, record, key) =>
        text==1?<Icon type="folder" />:
        text==2?<Icon type="file" />:
        text==3?<Icon type="table" />:
        text==4?<Icon type="dot-chart" />:null
      },
      {
        title: '特征表',
        dataIndex: 'name',
        align: 'left',
        width: 120,
        sorter: true,
        render: (text, record, key) =>
          record.editable ? (
            <div className={styles.iptSet}>
              <Input
                value={this.state.modifyText}
                ref={function(input) {
                  if (input != null) {
                    input.focus();
                  }
                }}
                onBlur={e => {
                  this.nameBlur(record, e, key);
                }}
                onChange={e => {
                  this.setState({
                    modifyText: e.target.value,
                  });
                }}
              />
              <span className={styles.iptSetSpan}>{record.errText}</span>
            </div>
          ) : (
            <span
              onClick={() => {
                console.log(record);
              }}
            >
              {record.name}
            </span>
          ),
      },
      {
        title: '描述',
        dataIndex: 'description',
        align: 'center',
        width: 200,
      },
      {
        title: '存储引擎',
        dataIndex: 'connectionType',
        align: 'center',
        width: 100,
      },
      {
        title: '创建人',
        dataIndex: 'createdBy',
        align: 'center',
        width: 80,
      },
      {
        title: '创建时间',
        dataIndex: 'createdTime',
        align: 'center',
        width: 160,
      },
      {
        title: '更新时间',
        dataIndex: 'updatedTime',
        align: 'center',
        width: 160,
      },

      {
        title: '操作',
        key: 'action',
        render: (text, record, key) => (
          <span>
            <Button
              className={styles.tabBtn}
              type="link"
              onClick={() => {
                router.push('/featureCenterList?id=' + record.id);
              }}
              disabled={!record.isDetail}
            >
              详情
            </Button>
            <Button
              className={styles.tabBtn}
              type="link"
              onClick={e => {
                e.persist();
                this.setListArr(record, e, key);
              }}
              disabled={!record.isUpdate}
            >
              重命名
            </Button>
            <Button
              className={styles.tabBtn}
              // disabled={record.tables}
              type="link"
              onClick={() => this.showModalmobile(record)}
              disabled={!record.isMove}
            >
              移动到
            </Button>
            <Button
              className={styles.tabBtn}
              type="link"
              onClick={() => this.deleteClick(record)}
              disabled={!record.isDelete}
            >
              删除
            </Button>
          </span>
        ),
        align: 'center',
        width: 200,
      },
    ];
    const pagination = {
      current: current,
      pageSize: size,
      total: total,
      showQuickJumper: true,
      onChange: (date, dateString) => {
        console.log(date);
        this.setState(
          {
            current: date,
          },
          this.search(date),
        );
      },
    };
    const expandedRowRender = e => {
      console.log(e);
      return (
        <Table columns={columns} dataSource={e.tables} pagination={false} showHeader={false} />
      );
    };
    return (
      <div className={styles.bmq}>
        <div className={styles.header}>
          <div className={styles.headerL}>特征表列表</div>
          <Button icon="plus" type="primary" onClick={this.showModal}>
          新增特征表
          </Button>
        </div>
        <div className={searchCss.search}>
          <div className={searchCss.searchItem}>
            <span>目录名称：</span>
            <Input
              className={searchCss.serachIpt}
              value={this.state.name}
              onChange={e => this.setState({ name: e.target.value })}
              placeholder="请输入目录名称"
              suffix={<Icon type="search" />}
            />
          </div>
          <div className={searchCss.searchItem}>
          <span>视图名称：</span>
          <Input
            className={searchCss.serachIpt}
            placeholder="请输入视图名称"
            value={this.state.queryView}
            onChange={e => this.setState({ queryView: e.target.value })}
            suffix={<Icon type="search" />}
          />
        </div>
        <div className={searchCss.searchItem}>
          <span>特征表名称：</span>
          <Input
            className={searchCss.serachIpt}
            placeholder="请输入特征表名称"
            value={this.state.queryTable}
            onChange={e => this.setState({ queryTable: e.target.value })}
            suffix={<Icon type="search" />}
          />
        </div>
        <div className={searchCss.searchItem}>
          <span>特征字段：</span>
          <Input
            className={searchCss.serachIpt}
            placeholder="请输入特征字段"
            value={this.state.queryField}
            onChange={e => this.setState({ queryField: e.target.value })}
            suffix={<Icon type="search" />}
          />
        </div>
         
          <div className={searchCss.searchItem}>
            <Button className={searchCss.r20} onClick={this.reset}>
              重置
            </Button>
            <Button type="primary" onClick={() => this.search()}>
              搜索
            </Button>
          </div>
        </div>
        <div className={searchCss.search2}>
        <div className={searchCss.searchItem}>
            <span>描述：</span>
            <Input
              className={searchCss.serachIpt}
              placeholder="请输入描述"
              value={this.state.description}
              onChange={e => this.setState({ description: e.target.value })}
              suffix={<Icon type="search" />}
            />
          </div>
          <div className={searchCss.searchItem}>
            <span>创建时间：</span>
            <RangePicker
              className={styles.picker}
              value={this.state.createdTime}
              onChange={this.onChangeTime1}
            />
          </div>

          <div className={searchCss.searchItem}>
            <span>更新时间：</span>
            <RangePicker
              className={styles.picker}
              value={this.state.updatedTime}
              onChange={this.onChangeTime2}
            />
          </div>

        </div>
        <div className={styles.table}>
          <Table
            columns={columns}
            // expandedRowRender={expandedRowRender}
            childrenColumnName="subset"
            dataSource={list}
            rowKey={"keyId"}
            expandedRowKeys={expandedRowKeys}
            onExpand={this.expandedRowClick}
            scroll={{ x: 1000 }}
            pagination={pagination}
          ></Table>
        </div>
        <Modal
          title="新增特征表"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              取消
            </Button>,
            <Button key="submit" type="primary" loading={loading} onClick={this.handleOk}>
              下一步
            </Button>,
          ]}
        >
          <Form {...formItemLayout}>
            <Form.Item label="目录">
              {featureList.id ? (
                <span className={styles.pointer} onClick={this.showModal1}>
                  {featureList.name}
                </span>
              ) : (
                <Button className={styles.paddl} type="link" onClick={this.showModal1}>
                  选择目录
                </Button>
              )}
            </Form.Item>
            <Form.Item label="数据类型">
              {getFieldDecorator('select', {
                initialValue: getEnum.length ? getEnum[0].id : '',
              })(
                <Select onChange={this.selectChange}>
                  {getEnum.length
                    ? getEnum.map(v => {
                        return <Option value={v.id}>{v.connectionName}</Option>;
                      })
                    : null}
                </Select>,
              )}
            </Form.Item>
            {(connectionConfig && connectionConfig.length)
            ? connectionConfig.map((item, index) => {
                return (
                  <Form.Item label={item.name}>{getFieldDecorator(item.field)(<Input />)}</Form.Item>
                )
            }, this)
          : null}
            {/* <Form.Item label="数据路径">{getFieldDecorator('description2')(<Input />)}</Form.Item> */}
          </Form>
        </Modal>

        <Modal
          title="选择目录"
          visible={this.state.visible1}
          onOk={this.handleOk1}
          onCancel={this.handleCancel1}
          zIndex={9999}
          width={1000}
          footer={[
            (visible1State==0?<div>
              <Button
              key="add1"
              style={{ float: 'left' }}
              disabled={listFolderState}
              onClick={this.addFolder}
            >
              新建文件夹
            </Button>
            <Button
              key="add2"
              style={{ float: 'left' }}
              disabled={listFolderState}
              onClick={this.addFolder2}
            >
              新建视图
            </Button>
            </div>:null),
            <Button key="back1" onClick={this.handleCancel1}>
              取消
            </Button>,
            <Button key="submit1" type="primary" loading={loading} onClick={this.handleOk1}>
              确定
            </Button>,
          ]}
        >
          {listFolder.length ? (
            <Table
              columns={columns1}
              dataSource={listFolder}
              rowKey={'keyId'}
              defaultExpandAllRows={true}
              pagination={false}
              scroll={{ y:  350}}
              rowSelection={{
                type: 'radio',
                onChange: this.onChange3,
                selectedRowKeys: this.state.selectedRowKeys,
              }}
              onRow={record => {
                return {
                  onClick: () => {
                    if(!listFolderState){
                      this.setState({
                        selectedRowKeys: [record.keyId],
                      });
                    }
                  }, // 点击行
                };
              }}
            ></Table>
          ) : (
            ''
          )}
        </Modal>
      </div>
    );
  }
}

export default FeatureCenter;
