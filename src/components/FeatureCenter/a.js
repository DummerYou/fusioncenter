let dataJsona = {
  "code": 200,
  "message": "请求成功",
  "data": [
    {
      "id": 1,
      "createdBy": "SYSTEM",
      "createdTime": "2021-06-25 22:55:55",
      "updatedTime": "2021-06-28 23:57:43",
      "updatedBy": "SYSTEM",
      "isDeleted": 0,
      "connectionType": "HDFS",
      "connectionConfig": "[{\"name\":\"连接地址\",\"field\":\"aiosUrl\",\"value\":\"\"},{\"name\":\"aiosWorkspaceId\",\"field\":\"aiosWorkspaceId\",\"value\":\"\"},{\"name\":\"表名\",\"field\":\"tableName\",\"value\":\"\"},{\"name\":\"文件地址\",\"field\":\"url\",\"value\":\"\"}]",
      "importFlag": 0
    },
    {
      "id": 2,
      "createdBy": "SYSTEM",
      "createdTime": "2021-06-25 23:53:50",
      "updatedTime": "2021-06-29 05:46:40",
      "updatedBy": "SYSTEM",
      "isDeleted": 0,
      "connectionType": "HIVE",
      "connectionConfig": "[{\"name\":\"aios连接地址\",\"field\":\"aiosUrl\",\"value\":\"\"},{\"name\":\"aiosWorkspaceId\",\"field\":\"aiosWorkspaceId\",\"value\":\"\"},{\"name\":\"hive连接地址\",\"field\":\"host\",\"value\":\"\"},{\"name\":\"hive连接端口\",\"field\":\"port\",\"value\":\"\"},{\"name\":\"用户名\",\"field\":\"username\",\"value\":\"\"},{\"name\":\"密码\",\"field\":\"password\",\"value\":\"\"},{\"name\":\"库名\",\"field\":\"dbname\",\"value\":\"\"},{\"name\":\"表名\",\"field\":\"table\",\"value\":\"\"}]",
      "importFlag": 0
    },
    {
      "id": 3,
      "createdBy": "SYSTEM",
      "createdTime": "2021-06-29 02:45:15",
      "updatedTime": "2021-06-29 05:46:40",
      "updatedBy": "SYSTEM",
      "isDeleted": 0,
      "connectionType": "AIOS",
      "connectionConfig": "[{\"name\":\"aios连接地址\",\"field\":\"aiosUrl\",\"value\":\"\"},{\"name\":\"aiosWorkspaceId\",\"field\":\"aiosWorkspaceId\",\"value\":\"\"},{\"name\":\"连接prn\",\"field\":\"aiosPrn\",\"value\":\"\"}]",
      "importFlag": 0
    }
  ]
}
export default dataJsona