// ref: https://umijs.org/config/
export default {
  treeShaking: true,
  routes: [
    {
      path: '/',
      component: '../pages/login/index',
    },
    {
      path: '/',
      component: '../layouts/index',
      routes: [{ path: '/index', component: '../pages/index' },
      { path: '/home', component: '../pages/home/index' },
      { path: '/featureCenterList', component: '../pages/featureCenterList/index'},
      { path: '/featuresNew', component: '../pages/featuresNew/index' },
      { path: '/BusinessMonitoringQuery', component: '../pages/BusinessMonitoringQuery/index' },
      { path: '/ResourceQuery', component: '../pages/ResourceQuery/index' }],
    },
  ],
  plugins: [
    // ref: https://umijs.org/plugin/umi-plugin-react.html
    [
      'umi-plugin-react',
      {
        antd: true,
        dva: true,
        dynamicImport: false,
        title: 'fusioncenter',
        dll: false,

        routes: {
          exclude: [
            /models\//,
            /services\//,
            /model\.(t|j)sx?$/,
            /service\.(t|j)sx?$/,
            /components\//,
          ],
        },
      },
    ],
  ],
  theme: {
    'primary-color': '#d4343c',
  },
  proxy: {
    '/urlAPI': {
      target: 'http://10.104.1.142:8001/',
      changeOrigin: true,
      pathRewrite: { '^/urlAPI': '' },
      cookieDomainRewrite: {
        '*': 'localhost',
      },
    },
    '/loginUrl': {
      target: 'http://10.104.1.141/',
      changeOrigin: true,
      pathRewrite: { '^/loginUrl': '' },
      cookieDomainRewrite: {
        '*': 'localhost',
      },
    },
  },
};
